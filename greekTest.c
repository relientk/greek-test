#if __EMSCRIPTEN__
#include <emscripten.h>
#endif
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <time.h>
#include <stdbool.h>
#define NUM_WORDS 15
#define MAX_WORDS (NUM_WORDS*3 + 1)
typedef struct Word
{
	const char* text;
	SDL_Texture* texture;
	SDL_Rect position;
} Word;
char *data, *dataPtr, *lengths, *lenPtr;
bool quit = false;
TTF_Font* greekFont;
SDL_Color white = {255,255,255};
SDL_Color red = {255,0,0};
SDL_Renderer* renderer;
SDL_Window* window;
SDL_Point tmp;
int screenWidth, screenHeight, spacing, nFound;
int maxLen = 0;
int length = -1;
int selectIndex;
Word *translit, *selectedWord;
Word word[MAX_WORDS];
bool Found[MAX_WORDS];
int fontSize;
float scaleX, scaleY;
void read_file(char** start, char** curr, const char* filename)
{
	SDL_RWops* data = SDL_RWFromFile(filename, "r");
	int Size = SDL_RWsize(data);
	*start = *curr = malloc(Size);
	SDL_RWread(data, *start, 1, Size);
	SDL_FreeRW(data);
}
void initialize()
{
	srand(time(NULL));
	SDL_Init(SDL_INIT_VIDEO);
	TTF_Init();
	window = SDL_CreateWindow("greekTest", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 1024, 768, 
			SDL_WINDOW_ALLOW_HIGHDPI | SDL_WINDOW_RESIZABLE);
	read_file(&data, &dataPtr, "res/data");
	read_file(&lengths, &lenPtr, "res/lengths");
	for(int i = 0; i < MAX_WORDS; i++) Found[i] = false;
	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
	int x, y;
	SDL_GetWindowSize(window, &x, &y);
	SDL_GetRendererOutputSize(renderer, &screenWidth, &screenHeight);
	scaleX = (float)screenWidth / x;
	scaleY = (float)screenHeight / y;
	greekFont = TTF_OpenFontRW(SDL_RWFromFile("res/cardo.ttf", "r"), 1, fontSize = screenHeight / NUM_WORDS);
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
}
void wind_down()
{
	free(data);
	free(lengths);
	for(int i = 0; i < maxLen; i++) SDL_DestroyTexture(word[i].texture);
	SDL_DestroyWindow(window);
	TTF_CloseFont(greekFont);
	TTF_Quit();
	SDL_Quit();
}
void gen_tex(Word* w, SDL_Color* color)
{
	SDL_Surface* surface = TTF_RenderUTF8_Blended(greekFont, w->text, *color);
	w->texture = SDL_CreateTextureFromSurface(renderer, surface);
	memcpy(&w->position.w, &surface->w, 2*sizeof(int));
	SDL_FreeSurface(surface);
}
void create_word(Word* w, const char* str, SDL_Color* color)
{
	w->text = str;
	gen_tex(w, color);
}
void regen_texes()
{
	Word* w = word;
	int i = 0;
	for(; i < length; i++) gen_tex(w++, &red);
	for(; i < 3*length+1; i++) gen_tex(w++, &white);
}
void render_scene()
{
	SDL_RenderClear(renderer);
	Word* w = word;
	for(int i = 0; i < 2*length + 1; i++, w++) SDL_RenderCopy(renderer, w->texture, NULL, &w->position);
	if(translit != NULL) SDL_RenderCopy(renderer, translit->texture, NULL, &translit->position);
	SDL_RenderPresent(renderer);
}
void reproportionate_screen(float x, float y)
{
	Word* w = word;
	for(int i = 0; i < length; i++, w++)
	{
		w->position.x = w->position.x * x;
		w->position.y = w->position.y * y;
	}
}
void gen_random_scatter()
{
	Word* w = word;
	for(int i = 0; i < length; i++, w++)
	{
		if(Found[i])
		{
			w->position.x = -100;
			w->position.y = -100;
		}
		else
		{
			w->position.x = rand() % (screenWidth - w->position.w);
			w->position.y = rand() % (screenHeight - w->position.h);  
		}
	}
}
void layout_scene()
{
	spacing = screenHeight / (length + 1);
	int currentSpace = -fontSize / 2;
	Word* w = word + length;
	Word* t = w + length + 1;
	int margin = t[-1].position.w;
	for(int i = 0; i < length; i++, w++, t++)
	{
		currentSpace += spacing;
		w->position.x = screenWidth - margin - w->position.w; 
		w->position.y = currentSpace;
		t->position.x = margin;
		t->position.y = currentSpace;
	}
	w->position.x = screenWidth - margin;
	w++->position.y = currentSpace;
}
void get_data_from_file()
{
	if(length != -1)
	{
		Word* w = word;
		for(int i = 0; i < 3*length+1; i++, w++) SDL_DestroyTexture(w->texture);
	}
	char* start;
	length      = *lenPtr++;
	maxLen      = maxLen > length ? maxLen : length;
	Word* e     =  word;
	Word* g     =  e + length;
	Word* t     =  g + length + 1;
	for(int i = 0; i < length; i++)
	{
		start = dataPtr;
		while(*++dataPtr != ' ');
		*dataPtr++ = '\0';
		create_word(t++, start, &white);
		start = dataPtr;
		while(*++dataPtr != ' ');
		*dataPtr++ = '\0';
		create_word(g++, start, &white);
		start = dataPtr;
		while(*++dataPtr != '\n');
		*dataPtr++ = '\0';
		if(*start == '-')
		{
			create_word(e++, " ", &red);
			++nFound;
		}
		else create_word(e++, start, &red);
	}
	start = dataPtr;
	*(dataPtr + 1) = '\0';
	dataPtr += 2;
	create_word(g, start, &white);
}
void reset_score()
{
	for(int i = 0; i < length; i++) Found[i] = false; 
	nFound = 0;
}
void next_scene()
{
	reset_score();
	get_data_from_file();
	gen_random_scatter();
	layout_scene();
	render_scene();
}
void tick()
{
	bool shouldRender = false;
	SDL_Event event;
	Word* w;
	int i;
GetNextEvent:
	while(SDL_PollEvent(&event))
	{
		switch(event.type)
		{
			case SDL_MOUSEBUTTONDOWN:
				w = word;
				tmp.x = event.motion.x * scaleX;
				tmp.y = event.motion.y * scaleY;
				for(i = 0; i < length; i++, w++)
				{
					if(SDL_PointInRect(&tmp, &w->position) == SDL_TRUE)
					{
						selectedWord = w;
						nFound -= (int)*(Found + i);
						*(Found + i) = false;	
						selectIndex = i;
						goto GetNextEvent;
					}
				}
				for(i = 0; i < length; i++, w++)
				{
					if(SDL_PointInRect(&tmp, &w->position) == SDL_TRUE)
					{
						translit = word + (2*length + 1 + i);
						shouldRender = true;
						goto GetNextEvent;
					}
				}
				break;
			case SDL_MOUSEMOTION:
				if(selectedWord != NULL)
				{
					selectedWord->position.x += event.motion.xrel * scaleX;
					selectedWord->position.y += event.motion.yrel * scaleY;
					shouldRender = true;
				}
				break;
			case SDL_MOUSEBUTTONUP:
				if(selectedWord != NULL)
				{
					shouldRender = true;
					int index = (selectedWord->position.y - spacing/2 + fontSize/2) / spacing;
					index = index < length ? index : length - 1;
					Word* w = word + index;
					selectedWord->position.y = (index + 1) * spacing - fontSize/2;
					{
						if(strcmp(w->text, selectedWord->text) == 0) 
						{
							*(Found + selectIndex) = true;
							++nFound;
							if(nFound == length)
							{
								next_scene();
							}
						}
					}
					selectedWord = NULL;
				}
				else if(translit != NULL)
				{
					translit = NULL;
					shouldRender = true;
				}
				break;
			case SDL_WINDOWEVENT:
				if(event.window.event == SDL_WINDOWEVENT_RESIZED)
				{
					int newWidth, newHeight;
					SDL_GetRendererOutputSize(renderer, &newWidth, &newHeight);
					reproportionate_screen((float)newWidth / screenWidth, (float)newHeight / screenHeight);
					screenWidth  = newWidth;
					screenHeight = newHeight;
					TTF_CloseFont(greekFont);
					greekFont = TTF_OpenFontRW(SDL_RWFromFile("res/cardo.ttf", "r"), 1, fontSize = screenHeight / NUM_WORDS);
					regen_texes();
					layout_scene();
				}
				shouldRender = true;
				break;
			case SDL_QUIT:
				wind_down();        
				quit = true;
				break;
		}
	}
	if(shouldRender) render_scene();
}
int main(int argc, char* argv[])
{
	initialize();
	next_scene();
#if __EMSCRIPTEN__
	emscripten_set_main_loop(tick, 60, 1);
#else
	while(!quit) tick();
#endif
}
