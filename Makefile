.DEFAULT_GOAL := em
CC=gcc
CFLAGS= -g -Wall
LDFLAGS=-lSDL2 -lSDL2_ttf

EM_CC=emcc
EM_CFLAGS=-s WASM=1 -O3
EM_LDFLAGS=-s USE_SDL=2 -s USE_SDL_TTF=2

pc:
	$(CC) $(CFLAGS) $(LDFLAGS) greekTest.c -o greekTest 
em:
	$(EM_CC) greekTest.c $(EM_CFLAGS) $(EM_LDFLAGS) --preload-files res -o out/index.js -s WASM=1

